import React from 'react';
import { Profile } from "../../app/models/profile";
import { Card,CardDescription,Icon,Image } from "semantic-ui-react";
import { observer } from "mobx-react-lite";
import { Link } from "react-router-dom";
interface Props {
    profile: Profile;
}
export default observer(function ProfileCard({profile}: Props){
    return(
    <Card as={Link} to={`/profile/${profile.username}`}>
        <Image src={profile.image || '/assets/user.png'} />
        <Card.Content>
            <Card.Header>
                {profile.displayName}
            </Card.Header>
            <CardDescription>Bio goes here</CardDescription>
        </Card.Content>
        <Card.Content extra>
            <Icon name= 'user' />
            20 followers
        </Card.Content>
            
        </Card>
    )
})