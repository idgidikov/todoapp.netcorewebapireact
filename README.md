# to start the project clone the repo 


# for backend part :

* open a terminal and while in the reactivities directory run the following command : dotnet run build 

* then in the API directory run : dotnet watch run

# for frontend part : 

* open a terminal in client-app directory run : npm i
* next : npm run dev 

* for password for user look a like : Pa$$w0rd
